# Belajar HTML

> Belajar [HTML Dasar](https://www.youtube.com/playlist?list=PLFIM0718LjIVuONHysfOK0ZtiqUWvrx4F) di Channel Youtube [Web Programming Unpas](https://www.youtube.com/channel/UCkXmLjEr95LVtGuIm3l2dPg)

## Apa saja yang kita pelajari?

1. [Hello, World!](https://www.youtube.com/watch?v=1NicaVOCXHA)
2. [Tag-tag pada HTML](https://www.youtube.com/watch?v=cUWBYzA6M-8)
3. [Paragraf pada HTML](https://www.youtube.com/watch?v=Dl_bIYBc9gM)
4. [Heading pada HTML](https://www.youtube.com/watch?v=SMetRBdIh-8)
5. [List pada HTML](https://www.youtube.com/watch?v=gLHEoeupIZs)
6. [Hyperlink](https://www.youtube.com/watch?v=QIlBOI-hTuA)
7. [Gambar pada HTML](https://www.youtube.com/watch?v=yb_emYhY3Pc)
8. [Tabel pada HTML](https://www.youtube.com/watch?v=7-QNafrXigs)
9. [Menggabungkan Tabel](https://www.youtube.com/watch?v=qs8G2XWf7Yk)
10. [Form pada HTML](https://www.youtube.com/watch?v=LQf_Jj7jbCI)
